import 'package:dio/dio.dart';
import 'network.dart';

class Network {
  int _timeOut = 10000; //10s
  Dio _dio;

  Network() {
    BaseOptions options = BaseOptions(connectTimeout: _timeOut, receiveTimeout: _timeOut);
    options.baseUrl = ApiConstant.apiHost;
    Map<String, dynamic> headers = Map();
   /*
    Http request headers.
    headers["content-type"] = "application/json";
   */
    options.headers = headers;
    _dio = Dio(options);
    _dio.interceptors.add(LogInterceptor(responseBody: true, requestBody: true));
  }

  Future<Response> get({String url, Map<String, dynamic> params = const {}}) async {
    try {
      Response response = await _dio.get(
        url,
        queryParameters: params,
        options: Options(responseType: ResponseType.json),
      );
      return getApiResponse(response);
    } on DioError catch (e) {
      //handle error
      print("DioError: ${e.toString()}");
      return getError(e);
    }
  }

  Future<Response> post({String url, Object body = const {}, String contentType = Headers.jsonContentType}) async {
    try {
      Response response = await _dio.post(
        url,
        data: body,
        options: Options(responseType: ResponseType.json, contentType: contentType),
      );
      return getApiResponse(response);
    } on DioError catch (e) {
      //handle error
      print("DioError: ${e.toString()}");
      return getError(e);
    }
  }

  Response getError(DioError e) {
    switch (e.type) {
      case DioErrorType.CANCEL:
      case DioErrorType.CONNECT_TIMEOUT:
      case DioErrorType.RECEIVE_TIMEOUT:
      case DioErrorType.SEND_TIMEOUT:
      case DioErrorType.DEFAULT:
        return Response(data: e.message);
      case DioErrorType.RESPONSE:
      default:
        return Response(data: e.message);
    }
  }

  Response getApiResponse(Response response) {
    return response;
  }
}
