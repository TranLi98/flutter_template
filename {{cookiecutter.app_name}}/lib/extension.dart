import 'package:dio/dio.dart';

extension AppResponse on Response {
  bool get isSuccess {
    return this.statusCode != null && this.statusCode == 200;
  }
}
